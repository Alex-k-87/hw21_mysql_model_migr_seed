<?php

require_once '../../vendor/autoload.php';
require_once '../../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('courses', function ($table) {
    $table->bigIncrements('id');
    $table->string('title', 255);
    $table->tinyInteger('amount', 3);
    $table->timestamps();
    $table->softDeletes();
});
