<?php

require_once '../../vendor/autoload.php';
require_once '../../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('lessons', function ($table){
    $table->bigIncrements('id');
    $table->unsignedBigInteger('group_id');
    $table->string('title', 255);
    $table->text('description');
    $table->data('lessons_date');
    $table->timestamps();
    $table->softDeletes();

    $table->foreign('group_id')->references('id')->on('groups');

});
