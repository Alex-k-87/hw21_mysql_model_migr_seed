<?php

require_once '../../vendor/autoload.php';
require_once '../../config/eloquent.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('groups', function ($table){
    $table->bigIncrements('id');
    $table->unsignedBigInteger('course_id');
    $table->data('started_date');
    $table->timestamps();
    $table->softDeletes();

    $table->foreign('course_id')->references('id')->on('courses');

});
