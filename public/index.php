<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';
require_once '../config/blade.php';

$container = new \Illuminate\Container\Container();
$dispatcher = new \Illuminate\Events\Dispatcher($container);
$router = new \Illuminate\Routing\Router($dispatcher, $container);

function view($view, array $data = [])
{
    global $blade;

    return $blade->make($view, $data)->render();
}

function router()
{
    global $router;

    return $router;
}

//$router->get('/', \App\Controller\HomeController::class)->name('home');
//$router->get('/blog', \App\Controller\BlogController::class)->name('blog');
//$router->get('/services', \App\Controller\ServicesController::class)->name('services');
//$router->get('/team', \App\Controller\TeamController::class)->name('team');
//$router->get('/contact-us', \App\Controller\ContactController::class)->name('contacts');

$router->get('/test', function () {
    $course = \App\Model\Course::find(1);
    echo "<h2>Groups</h2>";
    foreach ($course->groups()->latest()->get() as $group) {
//echo "<h6>Lessons:</h6>";
//        foreach ($group->lessons()->latest()->get() as $lesson) {
//              echo "<h6>{$lesson->title} {$lesson->descript} {$lesson->lessons_date}</h6>";
//        }
        echo "<h3> {$course->title} start: {$group->started_date} </h3>";
        $students = $group->students;
        echo "<h6>Count of students:{$students->count()}</h6>";
//       var_dump($group->lessons);
//       echo "<h6>{$lessons}</h6>>";
    }
    foreach ($group->students as $student) {
        echo "<h5>{$student->first_name} {$student->last_name} </h5>";
    }

    echo "<h6>Lessons:</h6>";
    foreach ($course->groups()->latest()->get() as $group) {
//        echo "<h5>{$course->title}</h5>";
        foreach ($group->lessons()->latest()->get() as $lesson) {

            echo "<h6>{$lesson->title} {$lesson->descript} {$lesson->lessons_date}</h6>";
        }
    }
});

$request = \Illuminate\Http\Request::createFromGlobals();
$response = $router->dispatch($request);
echo $response->getContent();