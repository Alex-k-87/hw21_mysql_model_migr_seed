<?php


namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Student extends Model
{
    use SoftDeletes;

    public function groups(){
        return $this->belongsToMany(Group::class);
    }
}