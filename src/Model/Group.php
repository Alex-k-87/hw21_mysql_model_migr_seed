<?php


namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Group extends Model
{
    use SoftDeletes;

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
    public function students()
    {
        return $this->belongsToMany(Student::class);
    }
    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }
}