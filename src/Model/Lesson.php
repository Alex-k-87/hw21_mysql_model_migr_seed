<?php


namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Lesson extends Model
{
    use SoftDeletes;

    public function groups()
    {
        return $this->belongsTo(Group::class);
    }
}