<?php


namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Course extends Model
{
    use SoftDeletes;

    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}