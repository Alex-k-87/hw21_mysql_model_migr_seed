<?php

namespace App\Controller;

class TeamController
{
    public function __invoke()
    {
        return view('team');
    }
}
